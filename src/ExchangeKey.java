import java.math.BigInteger;
import java.util.*;

public class ExchangeKey {
	private int y, secret;
	
	private BigInteger bigP;
	
	protected BigInteger yPowerSecret;
	protected BigInteger valuePowerSecret;
	
	public ExchangeKey(int y, int p, int secret) {
		this.y = y;
		setP(p);
		this.secret = secret;
	}
	
	public ExchangeKey() {
		this(0, 0, 0);
	}
	
	public void setP(int p) {
		bigP = new BigInteger("" + p);
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setSecret(int secret) {
		this.secret = secret;
	}
	
	int generateCodeToSend() {
		BigInteger bigY = new BigInteger("" + y);
		yPowerSecret = bigY.pow(secret);
		
		BigInteger result = yPowerSecret.mod(bigP);
		return result.intValue();
	}
	
	int generateCommonKey(int valueReceived) {
		BigInteger bVal = new BigInteger("" + valueReceived);
		valuePowerSecret = bVal.pow(secret);
		
		BigInteger result = valuePowerSecret.mod(bigP);
		return result.intValue();
	}
	
	private static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		int y, p;
		Prime prime = new Prime();
		int aliceSecret, bernardSecret;
		ExchangeKey exAlice, exBernard;
		int toBernard, toAlice;
		int aKey, bKey;
		
		Random ran = new Random();
		y = ran.nextInt(991) + 10;
		System.out.println(" shared value for Y by both Alice and Bernard is: " + y);
		p = prime.getPrimeGreaterOrEqual(y + 100);
		System.out.println(" shared value for P by both Alice and Bernard is: " + p);
		
		aliceSecret = prime.getPrimeGreaterOrEqual(ran.nextInt(990) + 10);
		bernardSecret = prime.getPrimeGreaterOrEqual(ran.nextInt(990) + 10);
		System.out.println("Alice secret is: " + aliceSecret + ".  Bernard secret is " + bernardSecret + ".");
		
		exAlice = new ExchangeKey(y, p, aliceSecret);
		exBernard = new ExchangeKey(y, p, bernardSecret);
		toBernard = exAlice.generateCodeToSend();
		System.out.println(" Value to send to Bernard: " + toBernard + " from: " + exAlice.yPowerSecret);
		toAlice = exBernard.generateCodeToSend();
		System.out.println(" Value to send to Alice:   " + toAlice + " from: " + exBernard.yPowerSecret);
		
		bKey = exBernard.generateCommonKey(toBernard);
		aKey = exAlice.generateCommonKey(toAlice);
		System.out.println("Key calculated by Bernard: " + bKey + " from: " + exBernard.valuePowerSecret);
		System.out.println("Key calculated by Alice:   " + aKey + " from: " + exAlice.valuePowerSecret);
		if(aKey == bKey)
			System.out.println("Yes it works !!!");
		else
			System.out.println("Oops... something wrong");
		
		y = getIntInput("Enter value for Y: ");
		
		p = 0;
		while(true) {
			p = getIntInput("Enter a value for prime number p: ");
			if(p <= y) {
				System.out.println("The prime number P must be > Y: " + y);
				continue;
			}
			if(prime.isPrime(p))
				break;
			
			System.out.println(p + " is NOT a prime number");
			System.out.println("May I suggest: " + prime.getPrimeGreaterOrEqual(p) + " ?");
		}
		
		aliceSecret = getIntInput("Enter Alice's secret number: ");
		bernardSecret = getIntInput("Enter Bernard's secret number: ");
		
		exAlice = new ExchangeKey(y, p, aliceSecret);
		exBernard = new ExchangeKey(y, p, bernardSecret);
		toBernard = exAlice.generateCodeToSend();
		toAlice = exBernard.generateCodeToSend();
		System.out.println("Message that Bernard sends to Alice: " + toAlice);
		System.out.println("Message that Alice sends to Bernard: " + toBernard);
		
		aKey = exAlice.generateCommonKey(toAlice);
		bKey = exBernard.generateCommonKey(toBernard);
		System.out.println("Key calculated by Bernard: " + bKey);
		System.out.println("Key calculated by Alice:   " + aKey);
		if(aKey == bKey)
			System.out.println("Yes it works !!!");
		else
			System.out.println("Oups... something wrong");
	}
	
	private static int getIntInput(String prompt) {
		while(true) {
			System.out.print(prompt);
			String str = scan.nextLine();
			try {
				int n = Integer.parseInt(str);
				if(n > 0)
					return n;
				System.out.println("the number should be > 0");
			}
			catch(Exception e) {
				System.out.println("Please enter an integer");
			}
		}
	}
}