import java.util.BitSet;

public class Prime {
	
	private static BitSet bitSet = new BitSet(1000);
	
	private static int max = 3;
	
	public boolean isPrime(int n) {
		if (n == 2)
			return true;
		if (n < 3 || n % 2 == 0)
			return false;
		
		if (n <= max)
			return !bitSet.get(n / 2);
		
		for (int i = 3; i <= n; i += 2) {
			if (i * 2 > n)
				break;
			
			if (!bitSet.get(i / 2)) {
				int multiple = max / i;
				multiple *= i;
				
				if (multiple <= i)
					multiple = i * 2;
				clearMultiple(i, multiple, n);
			}
		}
		
		max = n;
		
		return !bitSet.get(n / 2);
	}
	
	int getPrimeGreaterOrEqual(int n) {
		if (n % 2 == 0)
			++n;
		while (true) {
			if (isPrime(n))
				return n;
			n += 2;
		}
	}
	
	public void setNotPrime(int n) {
		if (n % 2 == 0)
			return;
		bitSet.set(n / 2, true);
	}
	
	private void clearMultiple(int prime, int multiple, int max) {
		while (multiple <= max) {
			setNotPrime(multiple);
			multiple += prime;
		}
	}
	
	public static void main(String[] args) {
		Prime prime = new Prime();
		for (int i = 101; i >= 3; i -= 2) {
			if (prime.isPrime(i))
				System.out.println("Is " + i + " is prime: ");
		}
		
		for (int i = 500; i < 2000; i += 50)
			System.out.println("The first prime number >= " + i + " is " + prime.getPrimeGreaterOrEqual(i));
	}

}